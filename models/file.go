package models

//File is a struct used for channels and routines (like a DTO)
type File struct {
	File         [][]string
	MessageError error
}

package main

import (
	h "gitlab.com/altairramos/ecore/handlers"
	s "gitlab.com/altairramos/ecore/server"
)

// Run with
//		go run .
// Send request with:
//		curl -F 'file=@/path/matrix.csv' "localhost:8080/echo"
func main() {
	h.Register()

	port := ":8080"
	s.StartWebServer(port)
}

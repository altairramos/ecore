package handlers

import (
	"net/http"

	m "gitlab.com/altairramos/ecore/models"
	s "gitlab.com/altairramos/ecore/services"
)

//processMatrix is a wrap function to call the service layer using channel
func processMatrix(w http.ResponseWriter, r *http.Request, c chan m.File) {
	c <- s.ProcessMatrix(w, r)
}

package handlers

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMultiplyHandler_should_return_the_product_of_the_integers_in_the_matrix(t *testing.T) {
	file, err := os.Open("../files/matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	multiplyHandler := &MultiplyHandler{}
	server := httptest.NewServer(multiplyHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "362880"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

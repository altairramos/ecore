package handlers

import (
	"fmt"
	"net/http"

	m "gitlab.com/altairramos/ecore/models"
	s "gitlab.com/altairramos/ecore/services"
)

//MultiplyHandler for processing multiply call
type MultiplyHandler struct{}

func (ih *MultiplyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ch := make(chan m.File)
	go processMatrix(w, r, ch)
	processedFile := <-ch

	if processedFile.MessageError != nil {
		w.Write([]byte(fmt.Sprintf("Something went wrong while processing your file: %s", processedFile.MessageError.Error())))
		return
	}

	fmt.Fprint(w, s.MultiplyMatrix(processedFile.File))
}

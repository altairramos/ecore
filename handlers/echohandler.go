package handlers

import (
	"fmt"
	"net/http"

	m "gitlab.com/altairramos/ecore/models"
	s "gitlab.com/altairramos/ecore/services"
)

//EchoHandler for processing echo call
type EchoHandler struct{}

func (eh *EchoHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ch := make(chan m.File)
	go processMatrix(w, r, ch)
	processedFile := <-ch

	if processedFile.MessageError != nil {
		w.Write([]byte(fmt.Sprintf("Something went wrong while processing your file: %s", processedFile.MessageError.Error())))
		return
	}

	fmt.Fprint(w, s.FormatMatrixResults(processedFile.File))
}

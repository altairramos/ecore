package handlers

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestEchoHandler_should_return_the_matrix_as_a_string_in_matrix_format(t *testing.T) {
	file, err := os.Open("../files/matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	echoHandler := &EchoHandler{}
	server := httptest.NewServer(echoHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "1,2,3\n4,5,6\n7,8,9\n"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

package handlers

import (
	"net/http"
)

//Register is a function that will register all http.Handle
func Register() {
	echoHandler := &EchoHandler{}
	http.Handle("/echo", echoHandler)

	invertHandler := &InvertHandler{}
	http.Handle("/invert", invertHandler)

	flattenHandler := &FlattenHandler{}
	http.Handle("/flatten", flattenHandler)

	sumHandler := &SumHandler{}
	http.Handle("/sum", sumHandler)

	multiplyHandler := &MultiplyHandler{}
	http.Handle("/multiply", multiplyHandler)
}

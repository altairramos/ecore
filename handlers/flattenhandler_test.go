package handlers

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestFlattenHandler_should_return_the_matrix_as_a_one_line_string_with_values_separated_by_commas(t *testing.T) {
	file, err := os.Open("../files/matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	flattenHandler := &FlattenHandler{}
	server := httptest.NewServer(flattenHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "1,2,3,4,5,6,7,8,9"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

package server

import (
	"log"
	"net/http"
)

//StartWebServer whill start the web server at specific port
func StartWebServer(port string) {
	log.Println("Server started and listening on port", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

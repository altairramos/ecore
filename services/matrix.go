package services

import (
	"encoding/csv"
	"errors"
	"fmt"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"

	m "gitlab.com/altairramos/ecore/models"
)

//ProcessMatrix is a function which will validate and process the matrix
func ProcessMatrix(w http.ResponseWriter, r *http.Request) m.File {
	file, header, err := r.FormFile("file")
	if err != nil {
		return m.File{
			MessageError: err,
		}
	}
	defer file.Close()

	if !isValidFileExtension(*header) {
		return m.File{
			MessageError: errors.New("File extension not allowed"),
		}
	}

	records, err := csv.NewReader(file).ReadAll()
	if err != nil {
		return m.File{
			MessageError: err,
		}
	}

	if !validateQuadraticMatrix(records) {
		return m.File{
			MessageError: errors.New("Non-quadratic matrix"),
		}
	}

	if !validateOnlyIntegersNumbersInMatrix(records) {
		return m.File{
			MessageError: errors.New("There's a non-integer number in the matrix"),
		}
	}

	return m.File{
		File:         records,
		MessageError: err,
	}
}

//Verifiy if file extension is allowed
func isValidFileExtension(header multipart.FileHeader) bool {
	fileExtension := header.Filename[len(header.Filename)-3:]
	if strings.ToLower(fileExtension) == "csv" {
		return true
	}
	return false
}

//Verifiy if is quadratic matrix
func validateQuadraticMatrix(matrix [][]string) bool {
	return len(matrix[0]) == len(matrix)
}

//Verify if there's a non-integer in matrix
func validateOnlyIntegersNumbersInMatrix(matrix [][]string) bool {
	xl := len(matrix[0])
	yl := len(matrix)

	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			if !isNumeric(matrix[i][j]) {
				return false
			}
		}
	}
	return true
}

//Verify if string is a valid integer number
func isNumeric(s string) bool {
	_, err := strconv.Atoi(s)
	return err == nil
}

//FormatMatrixResults returns a formated matrix as string with values separated by commas.
func FormatMatrixResults(matrix [][]string) string {
	var response string
	for _, row := range matrix {
		response = fmt.Sprintf("%s%s\n", response, strings.Join(row, ","))
	}
	return response
}

//InvertMatrix returns the matrix as a string in matrix format where the columns and rows are inverted.
func InvertMatrix(matrix [][]string) [][]string {
	xl := len(matrix[0])
	yl := len(matrix)
	result := make([][]string, xl)
	for i := range result {
		result[i] = make([]string, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = matrix[j][i]
		}
	}
	return result
}

//FlattenMatrix returns the matrix as a 1 line string, with values separated by commas.
func FlattenMatrix(matrix [][]string) string {
	xl := len(matrix[0])
	yl := len(matrix)

	values := []string{}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			values = append(values, matrix[i][j])
		}
	}
	return strings.Join(values, ",")
}

//SumMatrix returns the sum of the integers in the matrix.
func SumMatrix(matrix [][]string) string {
	xl := len(matrix[0])
	yl := len(matrix)

	value := 0
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			matrixNum, err := strconv.Atoi(matrix[i][j])
			if err != nil {
				fmt.Println("An error occur while processing SumMatrix function " + err.Error())
				continue
			}
			value = value + matrixNum
		}
	}
	return strconv.Itoa(value)
}

//MultiplyMatrix returns the product of the integers in the matrix
func MultiplyMatrix(matrix [][]string) string {
	xl := len(matrix[0])
	yl := len(matrix)

	value := 1
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			matrixNum, err := strconv.Atoi(matrix[i][j])
			if err != nil {
				fmt.Println("An error occur while processing MultiplyMaxtrix function " + err.Error())
				continue
			}
			value = (value * matrixNum)
		}
	}
	return strconv.Itoa(value)
}

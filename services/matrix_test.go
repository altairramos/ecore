package services

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

type TestTypeHandler struct{}

func (th *TestTypeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	processedFile := ProcessMatrix(w, r)

	if processedFile.MessageError != nil {
		w.Write([]byte(fmt.Sprintf("Something went wrong while processing your file: %s", processedFile.MessageError.Error())))
		return
	}
}

func TestProcessMatrix_should_show_message_when_file_is_not_sent(t *testing.T) {
	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	resp, err := http.Get(server.URL)
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Received non-200 response: %d\n", resp.StatusCode)
	}

	expected := "Something went wrong while processing your file: request Content-Type isn't multipart/form-data"
	actual, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		t.Fatal(err)
	}

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

func TestProcessMatrix_should_show_message_when_file_is_not_found(t *testing.T) {
	file, err := os.Open("../files/matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file_with_other_name", "matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "Something went wrong while processing your file: http: no such file"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

func TestProcessMatrix_should_show_message_when_matrix_is_non_quadratic(t *testing.T) {
	file, err := os.Open("../files/non-quadratic matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "non-quadratic matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "Something went wrong while processing your file: Non-quadratic matrix"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

func TestProcessMatrix_should_show_message_when_there_a_non_integer_number_in_matrix(t *testing.T) {
	file, err := os.Open("../files/matrix with non-int numbers.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix with non-int numbers.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "Something went wrong while processing your file: There's a non-integer number in the matrix"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

func TestProcessMatrix_should_show_message_when_file_extension_is_not_allowed(t *testing.T) {
	file, err := os.Open("../files/matrix.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.txt")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	actual, err := ioutil.ReadAll(response.Body)

	expected := "Something went wrong while processing your file: File extension not allowed"

	if expected != string(actual) {
		t.Errorf("Expected the message '%s'\n", expected)
	}
}

func TestValidateFileExtension_should_return_true_if_extension_is_valid(t *testing.T) {
	file, err := os.Open("../files/matrix.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.csv")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	_, header, _ := req.FormFile("file")

	if !isValidFileExtension(*header) {
		t.Errorf("Expected the value '%s'\n", "true")
	}
}

func TestIsValidFileExtension_should_return_false_if_extension_is_invalid(t *testing.T) {
	file, err := os.Open("../files/matrix.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var requestBody bytes.Buffer
	multiPartWriter := multipart.NewWriter(&requestBody)

	fileWriter, err := multiPartWriter.CreateFormFile("file", "matrix.txt")
	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Fatal(err)
	}

	multiPartWriter.Close()

	testHandler := &TestTypeHandler{}
	server := httptest.NewServer(testHandler)
	defer server.Close()

	req, err := http.NewRequest("POST", server.URL, &requestBody)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	_, header, _ := req.FormFile("file")

	if isValidFileExtension(*header) {
		t.Errorf("Expected the value '%s'\n", "false")
	}
}

func TestValidateQuadraticMatrix_should_return_true_if_matrix_is_quadratic(t *testing.T) {
	matrix := [][]string{{"1", "2"}, {"3", "4"}}
	if !validateQuadraticMatrix(matrix) {
		t.Errorf("Expected the value '%s'\n", "true")
	}
}

func TestValidateQuadraticMatrix_should_return_false_if_matrix_is_not_quadratic(t *testing.T) {
	matrix := [][]string{{"1", "2"}, {"3", "4"}, {"5", "6"}, {"7", ""}}

	if validateQuadraticMatrix(matrix) {
		t.Errorf("Expected the value '%s'\n", "false")
	}
}

func TestValidateOnlyIntegersNumbersInMatrix_should_return_true_if_matrix_contains_only_integer_elements(t *testing.T) {
	matrix := [][]string{{"1", "2", "3", "4"}, {"5", "6", "7", "8"}, {"9", "10", "11", "12"}, {"13", "14", "15", "16"}}

	if !validateOnlyIntegersNumbersInMatrix(matrix) {
		t.Errorf("Expected the value '%s'\n", "true")
	}
}

func TestValidateOnlyIntegersNumbersInMatrix_should_return_false_if_matrix_contains_no_integer_elements(t *testing.T) {
	matrix := [][]string{{"1", "2", "3.5", "4"}, {"5", "6", "7", "8"}, {"9", "10", "", "12"}, {"13", "14", "15", "16"}}

	if validateOnlyIntegersNumbersInMatrix(matrix) {
		t.Errorf("Expected the value '%s'\n", "false")
	}
}

func TestFormatMatrixResults_should_return_a_formated_matrix_as_string_with_values_separated_by_commas(t *testing.T) {
	matrix := [][]string{{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}

	actual := FormatMatrixResults(matrix)

	expected := "1,2,3\n4,5,6\n7,8,9\n"

	if expected != string(actual) {
		t.Errorf("Expected the value '%s'\n", expected)
	}
}

func TestInvertMatrix_should_return_matrix_as_matrix_format_where_the_columns_and_rows_are_inverted(t *testing.T) {
	matrix := [][]string{{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}

	actual := InvertMatrix(matrix)

	expected := [][]string{{"1", "4", "7"}, {"2", "5", "8"}, {"3", "6", "9"}}

	if !assertEq(actual, expected) {
		t.Errorf("Expected the value '%s'\n", expected)
	}
}

func TestFlattenMatrix_should_return_matrix_as_one_line_string_with_values_separated_by_commas(t *testing.T) {
	matrix := [][]string{{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}

	actual := FlattenMatrix(matrix)

	expected := "1,2,3,4,5,6,7,8,9"

	if expected != string(actual) {
		t.Errorf("Expected the value '%s'\n", expected)
	}
}

func TestSumMatrix_should_return_the_sum_of_the_integers_in_the_matrix(t *testing.T) {
	matrix := [][]string{{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}

	actual := SumMatrix(matrix)

	expected := "45"

	if expected != actual {
		t.Errorf("Expected the value '%s'\n", expected)
	}
}

func TestMultiplyMatrix_should_return_the_product_of_the_integers_in_the_matrix(t *testing.T) {
	matrix := [][]string{{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}

	actual := MultiplyMatrix(matrix)

	expected := "362880"

	if expected != actual {
		t.Errorf("Expected the value '%s'\n", expected)
	}
}

func TestIsNumeric_should_return_true_if_string_is_a_integer(t *testing.T) {
	number := "5"

	if !isNumeric(number) {
		t.Errorf("Expected the value '%s'\n", "true")
	}
}

func TestIsNumeric_should_return_false_if_string_is_not_a_integer(t *testing.T) {
	number := "X"

	if isNumeric(number) {
		t.Errorf("Expected the value '%s'\n", "false")
	}
}

func assertEq(test [][]string, ans [][]string) bool {
	return reflect.DeepEqual(test, ans)
}

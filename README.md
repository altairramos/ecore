# e-CORE

# This is a technical  test for e-Core company.
GitLab URL: https://gitlab.com/altairramos/ecore
(ask for permission if you need)

# Instructions to build, run, test and documentation:
In the command prompt. under project directory, type the following commands:

Compile solution
```
go build "gitlab.com/altairramos/ecore"
```

Run web server
```
go run "gitlab.com/altairramos/ecore"
```

Run tests
```
go test ./... -v -cover
```

Documentation
```
go doc "gitlab.com/altairramos/ecore/handlers"
go doc "gitlab.com/altairramos/ecore/services"
go doc "gitlab.com/altairramos/ecore/server"
```

# Instructions to send a curl request (in my case I used Postman)

1. Echo
Send request
```
curl -F 'file=@/path/matrix.csv' "localhost:8080/echo"
```

2. Invert
Send request
```
curl -F 'file=@/path/matrix.csv' "localhost:8080/invert"
```

3. Flatten
Send request
```
curl -F 'file=@/path/matrix.csv' "localhost:8080/flatten"
```

4. Sum
    Send request
```
curl -F 'file=@/path/matrix.csv' "localhost:8080/sum"

```
5. Multiply
    Send request
```
curl -F 'file=@/path/matrix.csv' "localhost:8080/multiply"

```